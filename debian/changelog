liblarch (3.2.0-4) unstable; urgency=medium

  * Add explicit Build-Depends python3-setuptools because it is not
    part of Python 3.12 anymore (Closes: #1080642)
  * Bump standard version to 4.7.0

 -- Francois Mazen <mzf@debian.org>  Thu, 05 Sep 2024 20:00:20 +0200

liblarch (3.2.0-3) unstable; urgency=medium

  * Replace python3-nose to python3-pytest (Closes: #1018398)

 -- Francois Mazen <francois@mzf.fr>  Wed, 12 Oct 2022 19:09:12 +0200

liblarch (3.2.0-2) unstable; urgency=medium

  * Team upload
  * Fix maintainer according to adoption

 -- Bastian Germann <bage@debian.org>  Mon, 11 Jul 2022 13:31:10 +0200

liblarch (3.2.0-1) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Francois Mazen ]
  * Package reintroduction (Closes: #1014194)
  * New upstream version (Closes: #885379, #936887, #1001820)
  * Set debhelper compat to 13
  * Set Rules-Requires-Root no

 -- Francois Mazen <francois@mzf.fr>  Fri, 01 Jul 2022 21:41:54 +0200

liblarch (3.0-2) experimental; urgency=medium

  * New Maintainer (Closes: #755298). Thanks for Luca Falavigna for your work.
  * debian/control:
      - Added xauth and xvfb to Build:Depends field.
      - Organized alphabetically Build:Depends: field.
  * debian/examples:
      - Added line to include examples files.
  * debian/rules:
      - Changed test-args to use xvfb-run in test suite.
      - Included override_dh_compress to not compress examples files.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Mon, 21 Sep 2015 14:47:00 -0300

liblarch (3.0-1) experimental; urgency=medium

  * New upstream release.
  * debian/control:
    - Build python3-liblarch package instead of python-liblarch. Adjust
      (build-)dependencies accordingly to support Python 3 only.
    - Build-depend on python3-nose, python3-gi and gir1.2-gtk-3.0
      for testsuite support.
    - Remove XS-Testsuite field, not needed anymore with recent dpkg.
  * debian/docs:
    - README renamed to README.md in upstream tarball.
  * debian/examples:
    - Install main.py example program.
  * debian/rules:
    - Support build of Python 3 module only.
    - Run testsuite at build time.
  * debian/tests/control:
    - Reference python3-smoketest and python3-liblarch instead of
      their older Python 2 versions.
  * debian/tests/python3-smoketest:
    - Rename from python2-smoketest, and adjust to work with Python 3.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 02 Jun 2015 15:05:08 +0200

liblarch (2.1.0-3) unstable; urgency=medium

  * debian/tests/python2-smoketest:
    - Provide a simple test suite based on autopkgtest.
  * debian/control:
    - Do not provide python-liblarch-gtk transitional package anymore.
    - Build-depend against dh-python for Pybuild support.
    - Adjust Homepage field to match new URL.
    - Bump Standards-Version to 3.9.6, no changes required.
  * debian/copyright:
    - Update copyright years.
  * debian/rules:
    - Build with Pybuild.

 -- Luca Falavigna <dktrkranz@debian.org>  Fri, 01 May 2015 16:28:02 +0200

liblarch (2.1.0-2) unstable; urgency=low

  * Upload to unstable.
  * debian/control:
    - Use canonical URIs for VCS fields.
  * debian/watch:
    - Point to GitHub repository, thanks Bart Martens!

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 05 May 2013 14:22:10 +0200

liblarch (2.1.0-1) experimental; urgency=low

  * New upstream release.
  * debian/control:
    - Fix Vcs-* fields links.
    - Breaks gtg older than version 0.3.
    - Provide transitional package for python-liblarch-gtk, which has
      been merged in python-liblarch.
    - Bump Standards-Version to 3.9.4, no changes required.
  * debian/copyright:
    - Point to 1.0 copyright format URL.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 10 Nov 2012 10:31:26 +0100

liblarch (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #660142).

 -- Luca Falavigna <dktrkranz@debian.org>  Thu, 16 Feb 2012 20:52:22 +0100
